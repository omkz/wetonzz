var http = require('http');
var url = require('url');
var QS = require('querystring');
var jsdom = require('jsdom');

//var url = 'http://www.islamicity.com/prayertimes/nprayvb4.asp?daylgt=N&stdate=&endate=&dstgroup=0&citydisplay=Yogyakarta&statedisplay=DI+Yogyakarta&countrydisplay=Indonesia&zipdisplay=n%2Fa&searchmode=ByCity&city=Yogyakarta&gmt=7&latd=007&latm=48&latS=S&longd=110&longm=22&longW=E&id=6742648&zipcode=&intvl=0&rad1=0&Month7=0&Year7=0&MonthQ=1&YearQ=0&YearY=0&hm7=9&hy7=1431&calculate=Calculate+Prayer+Times+for+Yogyakarta%2C+DI+Yogyakarta%2C+Indonesia';


app.message(function (client, action, data) {
    if (action === 'search') {
        var param = {
            'tg':data.tg,
            'bl':data.bl,
            'th':data.th
        };
        param = QS.stringify(param);


        var siteUrl = url.parse('http://ki-demang.com/php_files/kds10_02wetonproses.php');

        var headers = {
            'Host':siteUrl.host,
            'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2',
            'Content-Type':'application/x-www-form-urlencoded',
            'Content-Length':param.length

        };

        var site = http.createClient(siteUrl.port || 80, siteUrl.host);

        var request = site.request("POST", siteUrl.pathname, headers);
        request.write(param);
        request.end();

        request.on('response', function (response) {
            response.setEncoding('utf8');
            console.log('Status: ' + response.statusCode);
            response.on('data', function (data) {
                console.log("Data Masuk => " + data);

                jsdom.env(data,
                    [
                        'http://code.jquery.com/jquery-1.5.min.js'
                    ],
                    function (errors, window) {
                        var dataContent;
                        var dataHeader;
                        window.$('.stylekds13 tr').each(function () {
                            window.$(this).find("td").each(function () {
                                    console.log('Data Header => ' + window.$(this).text());
                                    dataHeader = window.$(this).text();
                                    client.msg('search',
                                        {text:{
                                            header:dataHeader }
                                        });
                                }
                            )
                                ;
                        });

                        window.$('.stylekds7 tr').each(function () {
                            window.$(this).find("td").each(function () {
                                console.log('Data Content => ' + window.$(this).text());
                                dataContent = window.$(this).text();
                                client.msg('search',
                                    {text:{
                                        content:dataContent }
                                    });
                            });

                        });

                    }
                );


            });
        });

    }
    if (action === 'wuku') {
        var param2 = {
            'tg':data.tg,
            'bl':data.bl,
            'th':data.th
        };
        param2 = QS.stringify(param2);

        //host : http://ki-demang.com/php_files/02%20kalender%20weton%20on%20line.php
        var siteUrl2 = url.parse('http://ki-demang.com/php_files/kds11_02pawukonproses.php');

        //var siteUrl = url.parse('http://ki-demang.com/php_files/kds09_02penanggalanproses.php');
        var headers2 = {
            'Host':siteUrl2.host,
            'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2',
            'Content-Type':'application/x-www-form-urlencoded',
            'Content-Length':param2.length

        };

        var site2 = http.createClient(siteUrl2.port || 80, siteUrl2.host);

        var request2 = site2.request("POST", siteUrl2.pathname, headers2);
        request2.write(param2);
        request2.end();

        request2.on('response', function (response2) {
            response2.setEncoding('utf8');
            console.log('Status: ' + response2.statusCode);
            response2.on('data', function (data) {
                console.log("Data Masuk => " + data);
                jsdom.env(data,
                    [
                        'http://code.jquery.com/jquery-1.5.min.js'
                    ],
                    function (errors, window) {
                        var dataContent;
                        var dataHeader;
                        window.$('.stylekds13 tr').each(function () {
                            window.$(this).find("td").each(function () {
                                    console.log('Data Header => ' + window.$(this).text());
                                    dataHeader = window.$(this).text();
                                    client.msg('search',
                                        {text:{
                                            header:dataHeader }
                                        });
                                }
                            )
                                ;
                        });

                        window.$('.stylekds7 tr').each(function () {
                            window.$(this).find("td").each(function () {
                                console.log('Data Content => ' + window.$(this).text());
                                dataContent = window.$(this).text();
                                client.msg('search',
                                    {text:{
                                        content:dataContent }
                                    });
                            });

                        });

                    }
                );


            });
        });

    }

    if (action === 'selametan') {
        var param5 = {
            'tg':data.tg,
            'bl':data.bl,
            'th':data.th
        };
        param5 = QS.stringify(param5);

        //host : http://ki-demang.com/php_files/02%20kalender%20weton%20on%20line.php
        var siteurl5 = url.parse('http://ki-demang.com/php_files/kds14_02selametanproses.php');

        //var siteUrl = url.parse('http://ki-demang.com/php_files/kds09_02penanggalanproses.php');
        var headers5 = {
            'Host':siteurl5.host,
            'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2',
            'Content-Type':'application/x-www-form-urlencoded',
            'Content-Length':param5.length

        };

        var site5 = http.createClient(siteurl5.port || 80, siteurl5.host);

        var request5 = site5.request("POST", siteurl5.pathname, headers5);
        request5.write(param5);
        request5.end();

        request5.on('response', function (response5) {
            response5.setEncoding('utf8');
            console.log('Status: ' + response5.statusCode);
            response5.on('data', function (data) {
                console.log("Data Masuk => " + data);
                jsdom.env(data,
                    [
                        'http://code.jquery.com/jquery-1.5.min.js'
                    ],
                    function (errors, window) {
                        var dataContent;
                        var dataHeader;
                        window.$('.stylekds13 tr').each(function () {
                            window.$(this).find("td").each(function () {
                                    console.log('Data Header => ' + window.$(this).text());
                                    dataHeader = window.$(this).text();
                                    client.msg('search',
                                        {text:{
                                            header:dataHeader }
                                        });
                                }
                            )
                                ;
                        });

                        window.$('.stylekds7 tr').each(function () {
                            window.$(this).find("td").each(function () {
                                console.log('Data Content => ' + window.$(this).text());
                                dataContent = window.$(this).text();
                                client.msg('search',
                                    {text:{
                                        content:dataContent }
                                    });
                            });

                        });

                    }
                );


            });
        });

    }
});


app.setResourceHandler(function (request, response) {

    app.debug('Client requested resource-id=' + request.id);

    function sendReply(response, error, imageType, data) {
        if (error) {
            app.warn('Failed to load image: ' + error);
            response.failed();
        } else {
            app.debug('Loaded image.');
            response.reply(imageType, data);
        }
    }

    scaling.scale(request.id, request.display_width, request.display_height, 'image/jpeg',
        function (err, data) {
            sendReply(response, err, 'image/jpeg', data);
        }
    );
});

