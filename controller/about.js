var _ = require('common/util');
var ImageView = require('ui').ImageView;
var app = this;

_.extend(exports, {
    ':load': function() {

    }    ,
    ':keypress'
           :
           function(key) {
               console.log('Key press: ' + key);
               if (this.index === undefined) {
                   if (this.size() >= 0) {
                       this.focusItem(0);
                   }
               } else if (key === 'up' || key === 'down') {
                   var next = this.index + (key === 'up' ? -1 : 1);

                   if (next < 0) {
                       next = 0;
                   } else if (next > (this.size() - 1)) {
                       next = this.size() - 1;
                   }

                   if (this.index === next) {
                       return;
                   }

                   this.focusItem(next);
               } else if (key === 'fire') {
                   this.get(this.index).emit('activate');
               }
           },

       ':active': function() {
           console.log('View is active');
       },

       ':inactive': function() {
           console.log('View is inactive');
       },

       focusItem: function(index) {
           if (this.index !== undefined) {
               this.get(this.index).emit('blur');
           }
           this.index = index;
           this.get(index).emit('focus');
           if (index === 1) {
               this.scrollTop(0);
           }
           console.log(index);
           this.scrollTo(index);
       }
});
