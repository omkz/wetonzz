var _ = require('common/util');
var ui = require('ui');
var TextView = require('ui').TextView;
var VLayout = require('ui').VLayout;
var Control = require('ui').Control;
_.extend(exports, {
    ':load': function() {
        console.log('View was loaded');
        var self = this;
        self.clear();
        var title = new TextView({
            label: 'Hasil Ramalan : ',
            style: {
                color: 'black',
                width:'fill-parent',
                height:'wrap-content',
                'font-size': 'normal',
                'font-weight': 'bold',
                'background-color':'#708090',
                border: '4 4 4 4'
            }
        });

        self.add(title);
        app.on("message", function(action, data) {
            if (action === 'search') {

                var dataArray = [data.text.content];
                var i = 1;
                dataArray.forEach(function(item) {

                    temp = new TextView({
                        label: item,
                        color: 'black',
                        width: 'fill-parent'
                    });
//                    console.log(data.text.content);
//                    console.log(data.text.header);
                    self.add(item, temp);
                    i++;
                });
            }
        });
        self.focusItem(0);
    }
    ,
    'state'
        :
        function(param) {
            var self = this;
            self.clear();
            self.scrollTop(0);


            var wait = new TextView({
                "label": "Silahkan Tunggu",
                "style": {
                    "border": "5 0 0 0"
                }
            });
            self.add(title);
            self.add('wait', wait);
            self.intervalId = setInterval(function() {
                if (self.id === undefined) {
                    self.id = 1;
                } else if (self.id < 10) {
                    self.id++;
                } else {
                    self.id = 1;
                }
                var temp = '';
                for (var i = 0; i < self.id; i++) {
                    temp = temp + '.';
                }

                self.get('wait').label('Silahkan Tunggu' + temp);
            }, 500);
            app.msg('getMenu', {action:'getMenu', url: param.url});
        }

    ,

    ':keypress'
        :
        function(key) {
            console.log('Key press: ' + key);
            if (this.index === undefined) {
                if (this.size() >= 0) {
                    this.focusItem(0);
                }
            } else if (key === 'up' || key === 'down') {
                var next = this.index + (key === 'up' ? -1 : 1);

                if (next < 0) {
                    next = 0;
                } else if (next > (this.size() - 1)) {
                    next = this.size() - 1;
                }

                if (this.index === next) {
                    return;
                }

                this.focusItem(next);
            } else if (key === 'fire') {
                this.get(this.index).emit('activate');
            }
        },

    ':active': function() {
        console.log('View is active');
    },

    ':inactive': function() {
        console.log('View is inactive');
    },

    focusItem: function(index) {
        if (this.index !== undefined) {
            this.get(this.index).emit('blur');
        }
        this.index = index;
        this.get(index).emit('focus');
        if (index === 1) {
            this.scrollTop(0);
        }
        console.log(index);
        this.scrollTo(index);
    }
});