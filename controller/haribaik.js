var _ = require('common/util');
var ImageView = require('ui').ImageView;
var app = this;

_.extend(exports, {
    ':load': function() {
        var self = this;

        self.selection = self.keySelectionWithItems([
            self.get('ibTanggal'),
            self.get('ibBulan'),
            self.get('ibTahun'),
            self.get('button-box').get('bSearch')
        ],
            {
                focusedCallback: function(item) {
                    if (item instanceof ImageView) {
                        self.get('button-box').get('bSearch').src(app.resourceURL('search_ho.png'));
                    } else {
                        self.get('button-box').get('bSearch').src(app.resourceURL('search.png'));
                    }

                }
            });

        self.get('button-box').get('bSearch').src(app.resourceURL('search_ho.png'));
        self.get('button-box').get('bSearch').src(app.resourceURL('search.png'));

        self.get('ibTanggal').on('activate', function() {
            self.get('ibTanggal').emit('keypress', 'fire');
        });

        self.get('ibBulan').on('activate', function() {
            self.get('ibBulan').emit('keypress', 'fire');
        });
        self.get('ibTahun').on('activate', function() {
            self.get('ibTahun').emit('keypress', 'fire');
        });

        self.get('button-box').get('bSearch').on('activate', function() {
            app.msg('search', {
                tg: self.get('ibTanggal').value(),
                bl: self.get('ibBulan').value(),
                th: self.get('ibTahun').value()
            });

            app.setContent('details', {
                header_ : data.text.header,
                content_ : data.text.content
            });


        });


        app.on('message', function(action, data) {
            if (action === 'search') {
                console.log('Data content => ' + data.text.content);
                console.log('Data header => ' + data.text.header);
            }
        });

    },
    'state' : function(param) {
        var self = this;
        self.clear();
        self.scrollTop(0);


        var wait = new TextView({
            "label": "Silahkan Tunggu",
            "style": {
                "border": "5 0 0 0"
            }
        });
        self.add(title);
        self.add('wait', wait);
        self.intervalId = setInterval(function() {
            if (self.id === undefined) {
                self.id = 1;
            } else if (self.id < 10) {
                self.id++;
                self.id++;
            } else {
                self.id = 1;
            }
            var temp = '';
            for (var i = 0; i < self.id; i++) {
                temp = temp + '.';
            }

            self.get('wait').label('Silahkan Tunggu' + temp);
        }, 500);

    }
});
