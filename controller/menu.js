var _ = require('common/util');
var ui = require('ui');
var InputBox = ui.InputBox;
var ImageView = ui.ImageView;
var Panels = require('ui/panels').Panels;
var HLayout = ui.HLayout;
var VLayout = ui.VLayout;
var CellLayout = ui.CellLayout;
var TextView = ui.TextView;

_.extend(exports, {
    ':load': function() {
        console.log('View was loaded');
        var p = new Panels();
        p.add('Watak Weton', 'main');
        //p.add('Watak Weton', 'watakwe');
        p.add('Watak Wuku', 'wuku');
        //p.add('Hari baik', 'haribaik');
        p.add('Tanggal Selametan', 'selametan');
        p.add('Tentang Aplikasi', 'about');
        this.add(p);
    },
    ':keypress': function(key) {
        console.log('Key press: ' + key);
        this.get(0)[':keypress'](key);
    }
});
